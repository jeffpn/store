@extends('template')
@section('conteudo')
<div class="container py-5">
    <h1> Listar produtos <a href="{{url('cadastrar-produto')}}" class="btn btn-outline-success float-right">Novo
            produto</a></h1>
    <p class="text-success">{{session('success')}}</p>
    <p class="text-danger">{{session('error')}}</p>
    <table class="table  table-striped">
        <thead>
            <tr>
                <th>Nome</th>
                <th>Preço</th>
                <th>Código de barras</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tbody>
            @foreach($produtos as $produto)
            <tr>
                <td>{{$produto->nome}}</td>
                <td>{{$produto->preco}}</td>
                <td>{{$produto->codigo_barra}}</td>
                <td>
                    <a href="{{url('editar-produto/'.$produto->slug)}}" class="btn btn-outline-primary
                         mr-2 py-0 px-2 mb-2 float-left" style="font-size: 1.2em;"><i class="far fa-edit"></i></a>
                    <form action="{{url('excluir-produto')}}" method="POST">
                        @csrf
                        <input name="slug" type="hidden" value="{{$produto->slug}}">
                        <button type="submit" class="btn btn-outline-danger mr-2 py-0 px-2" style="font-size: 1.2em;"><i
                                class="far fa-trash-alt"></i></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <h2 class=" font-weight-light">@if($produtos->isEmpty()) Nenhum produto cadastrado! @endif</h2>
</div>
@endsection
