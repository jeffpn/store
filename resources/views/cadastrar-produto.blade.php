@extends('template')

@section('conteudo')
<div class="container py-5">
    <h1> Cadastrar Produto <a href="{{url('listar-produtos')}}" class="btn btn-outline-primary float-right">Voltar</a>
    </h1>
    <p class="text-success">{{session('success')}}</p>
    <p class="text-danger">{{session('error')}}</p>
    <form action="/registrar-produto" method="POST">
        @csrf
        <input class="form-control my-2" type="text" name='nome' placeholder='Nome *' value="{{old('nome')}}">
        <p class="text-danger my-2">{{$errors->first('nome')}}</p>
        <input class="form-control my-2" tyep='text' name='codigo_barra' placeholder="Código de barra *"
            value="{{old('codigo_barra')}}">
        <p class="text-danger my-2">{{$errors->first('codigo_barra')}}</p>
        <textarea class="form-control my-2" name="descricao" rows="3"
            placeholder="Descrição *">{{old('descricao')}}</textarea>
        <p class="text-danger my-2">{{$errors->first('descricao')}}</p>
        <select class="form-control my-2" name="tipo_id">
            <option value=""> Escolha um tipo </option>
            @foreach($tipos as $tipo)
            <option value="{{$tipo->id}}" @if(old('tipo')==$tipo->id) selected @endif> {{$tipo->nome}} </option>
            @endforeach
        </select>
        <p class="text-danger my-2">{{$errors->first('tipo_id')}}</p>
        <input class="form-control my-2" type="text" name='preco' placeholder="Preço *" value="{{old('preco')}}">
        <p class="text-danger my-2">{{$errors->first('preco')}}</p>
        <button type="submit"> Salvar </button>
    </form>
</div>
@endsection
