<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tipo extends Model
{
    protected $fillable = ['nome'];
    public $timestamps = true;

    public function produtos()
    {
        return $this->hasMany('App\Models\Produto', 'tipo_id');
    }
}
