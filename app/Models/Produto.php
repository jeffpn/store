<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtos';

    protected $fillable = [
        'slug', 'nome', 'tipo_id', 'preco',
        'descricao', 'codigo_barra'
    ];
    public $timestamps = true;

    public function tipo()
    {
        return $this->hasOne('App\Models\Tipo', 'id');
    }
}
